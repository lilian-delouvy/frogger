#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h> //import de la bibliotheque SDL
#include "Header.h"
#include <FMOD/fmod.h>
#include <time.h>

TabObs to;

void afficheVictoire(Joueur J,SDL_Surface *screen, const SDL_VideoInfo *Resolution, int *continuer){
    SDL_EventState(SDL_MOUSEMOTION,SDL_DISABLE);
    SDL_Surface *victory = (J.numJ == 1) ? SDL_LoadBMP("VictoireJ1.bmp") : SDL_LoadBMP("VictoireJ2.bmp");
    SDL_Event Event;
    SDL_Rect centre;
    centre.x=((Resolution->current_w)/2-(1024/2));
    centre.y=((Resolution->current_h)/2-(575/2));

    SDL_FillRect(screen,NULL,SDL_MapRGB(screen->format,153,217,234));
    SDL_BlitSurface(victory,NULL,screen,&centre);
    SDL_Flip(screen);
    do{
        SDL_WaitEvent(&Event);
    }while(Event.type==SDL_MOUSEMOTION || Event.type== SDL_MOUSEBUTTONDOWN || Event.type == SDL_MOUSEBUTTONUP);
    if(SDL_WaitEvent(&Event)){
        switch(Event.type){
            case SDL_QUIT:
                *continuer = 0;
            break;
            default:
                break;
        }
    SDL_FreeSurface(victory);
    SDL_WaitEvent(&Event); //on permet � l'utilisateur de rel�cher sa touche
    }
}

void getPlayerState(Joueur J, SDL_Surface *screen, SDL_Rect posMenu, int *continuer, const SDL_VideoInfo *Resolution){
    if(J.alive==0){ //mode 1 joueur
        SDL_Surface *deathscreen=SDL_LoadBMP("GameOver.bmp");
        *continuer=0;
        SDL_Event Event;
        posMenu.x=((Resolution->current_w)/2-(1024/2));
        posMenu.y=((Resolution->current_h)/2-(575/2));
        SDL_FillRect(screen,NULL,SDL_MapRGB(screen->format,153,217,234));
        SDL_BlitSurface(deathscreen, NULL, screen, &posMenu);
        SDL_Flip(screen);
        do{
            SDL_WaitEvent(&Event);
        }while(Event.type==SDL_MOUSEMOTION || Event.type== SDL_MOUSEBUTTONDOWN || Event.type == SDL_MOUSEBUTTONUP);
        if(SDL_WaitEvent(&Event)){  //on attend un simple appui sur une touche
            switch(Event.type){
                default:
                    break;
            }
        }
        SDL_FreeSurface(deathscreen);
    }
    else{       //on consid�re forc�ment un joueur vivant
        if(J.posJ.y == 0){   //si le joueur est arriv� le premier en haut
            afficheVictoire(J,screen,Resolution,continuer);
            *continuer=0;
        }
    }
}

void getPlayerState2Joueurs(Joueur *J, SDL_Surface *screen, int *continuer, const SDL_VideoInfo *Resolution){
    if(J->alive==0 && J->numJ == 1){ //mode 1 joueur
        J->alive=1;
        J->posJ.y = 22*TAILLE_CASE;
        J->posJ.x = 4*TAILLE_CASE;
    }
    if(J->alive==0 && J->numJ == 2){ //mode 1 joueur
        J->alive=1;
        J->posJ.y = 22*TAILLE_CASE;
        J->posJ.x = 12*TAILLE_CASE;
    }
    else{       //on consid�re forc�ment un joueur vivant
        if(J->posJ.y == 0){   //si le joueur est arriv� le premier en haut
            afficheVictoire(*J,screen,Resolution,continuer);
            *continuer=0;
        }
    }
}

void getPlayerState1Joueur(Joueur J, SDL_Rect *posj, SDL_Surface *screen, SDL_Rect posMenu, int *continuer, const SDL_VideoInfo *Resolution){
    if(J.alive==0){ //mode 1 joueur
        SDL_Surface *deathscreen=SDL_LoadBMP("GameOver.bmp");
        *continuer=0;
        SDL_Event Event;
        posMenu.x=((Resolution->current_w)/2-(1024/2));
        posMenu.y=((Resolution->current_h)/2-(575/2));
        SDL_FillRect(screen,NULL,SDL_MapRGB(screen->format,153,217,234));
        SDL_BlitSurface(deathscreen, NULL, screen, &posMenu);
        SDL_Flip(screen);
        do{
            SDL_WaitEvent(&Event);
        }while(Event.type==SDL_MOUSEMOTION || Event.type== SDL_MOUSEBUTTONDOWN || Event.type == SDL_MOUSEBUTTONUP);
        if(SDL_WaitEvent(&Event)){  //on attend un simple appui sur une touche
            switch(Event.type){
                default:
                    break;
            }
        }
        SDL_FreeSurface(deathscreen);
    }
    else{       //on consid�re forc�ment un joueur vivant
        if(posj->y == 0){   //si le joueur est arriv� le premier en haut
            posj->y = 22*TAILLE_CASE;
        }
    }
}

void deplacerJ(SDL_Rect *pos, int direction)
{
    switch(direction)
    {
        case GAUCHE: //GAUCHE = 0
            if (pos-> x-1 < 0) //si gauche = sortie de map
                break;
            pos->x-=TAILLE_CASE; //deplacement du joueur a gauche
            break;
        case BAS: //BAS = 1
            if (pos-> y+1 > H_FENETRE-32) //si bas = sortie de map
                break;
            pos->y+=TAILLE_CASE; //deplacement du joueur en bas
            break;
        case DROITE: //DROITE = 2
            if (pos-> x+1 > L_FENETRE-32) //si droite = sortie de map
                break;
            pos->x+=TAILLE_CASE; //deplacement du joueur a droite
            break;
        case HAUT: //HAUT = 3
            if (pos-> y-1 < 0) //si haut = sortie de map
                break;
            pos->y-=TAILLE_CASE; //deplacement du joueur en haut
            break;
    }
}

Obstacle creerObs(int type, int x, int y, int xpix, int ypix)
{
    Obstacle o;
    o.type = type;
    o.posx = x; o.posy = y;
    o.pospix.x = xpix; o.pospix.y = ypix;
    int s = (rand()*(6 - 1)/RAND_MAX) +1 ;
    switch (s)
    {
        case 1:
            switch(type)
            {
                case RONDIN:
                    o.image = SDL_LoadBMP("Rondin.bmp"); break;
                case NENUPHAR:
                    o.image = SDL_LoadBMP("Nenuphar.bmp"); break;
                case VOITUREG:
                    o.image = SDL_LoadBMP("CarG.bmp"); break;
                case VOITURED:
                    o.image = SDL_LoadBMP("CarD.bmp"); break;
                case EAU:
                    o.image = SDL_LoadBMP("Vide.bmp"); break;
                default:
                    break;
            }
            break;
        case 2:
            switch(type)
            {
                case RONDIN:
                    o.image = SDL_LoadBMP("Rondin.bmp"); break;
                case NENUPHAR:
                    o.image = SDL_LoadBMP("Nenuphar.bmp"); break;
                case VOITUREG:
                    o.image = SDL_LoadBMP("CarG2.bmp"); break;
                case VOITURED:
                    o.image = SDL_LoadBMP("CarD2.bmp"); break;
                case EAU:
                    o.image = SDL_LoadBMP("Vide.bmp"); break;
                default:
                    break;
            }
            break;
         case 3:
            switch(type)
            {
                case RONDIN:
                    o.image = SDL_LoadBMP("Rondin.bmp"); break;
                case NENUPHAR:
                    o.image = SDL_LoadBMP("Nenuphar.bmp"); break;
                case VOITUREG:
                    o.image = SDL_LoadBMP("CarG3.bmp"); break;
                case VOITURED:
                    o.image = SDL_LoadBMP("CarD3.bmp"); break;
                case EAU:
                    o.image = SDL_LoadBMP("Vide.bmp"); break;
                default:
                    break;
            }
            break;
        case 4:
            switch(type)
            {
                case RONDIN:
                    o.image = SDL_LoadBMP("Rondin.bmp"); break;
                case NENUPHAR:
                    o.image = SDL_LoadBMP("Nenuphar.bmp"); break;
                case VOITUREG:
                    o.image = SDL_LoadBMP("CarG4.bmp"); break;
                case VOITURED:
                    o.image = SDL_LoadBMP("CarD4.bmp"); break;
                case EAU:
                    o.image = SDL_LoadBMP("Vide.bmp"); break;
                default:
                    break;
            }
            break;
        case 5:
            switch(type)
            {
                case RONDIN:
                    o.image = SDL_LoadBMP("Rondin.bmp"); break;
                case NENUPHAR:
                    o.image = SDL_LoadBMP("Nenuphar.bmp"); break;
                case VOITUREG:
                    o.image = SDL_LoadBMP("CarG5.bmp"); break;
                case VOITURED:
                    o.image = SDL_LoadBMP("CarD5.bmp"); break;
                case EAU:
                    o.image = SDL_LoadBMP("Vide.bmp"); break;
                default:
                    break;
            }
            break;
        default:
            break;
    }
    SDL_SetColorKey(o.image, SDL_SRCCOLORKEY, SDL_MapRGB(o.image->format, 255, 0, 0));
    return o;
}

void initTabObsAleatoire(GrilleJeu t)
{
    int i, j, k = 0, alea, maxOb = 0;
    for(i=0; i<NB_CASES_L; i++) t[i][0] = SOL; //ligne 0
    srand((unsigned)time(NULL));
    while(maxOb < 10) //ligne 1
    {
        for(i=0; i<NB_CASES_L; i++)
        {
            alea = (rand()*(3 - 1)/RAND_MAX) +1;
            if (alea == 1 && t[i][1] != RONDIN && maxOb<10)
            {
                to[k] = creerObs(RONDIN, i, 1, i*TAILLE_CASE, TAILLE_CASE); k++; maxOb++;
                t[i][1] = RONDIN;
            }
        }
    }
    for(i=0; i<NB_CASES_L; i++)
    {
        if(t[i][1] != RONDIN)
        {
            to[k] = creerObs(EAU, i, 1, i*TAILLE_CASE, TAILLE_CASE); k++;
            t[i][1] = EAU;
        }
    }
    maxOb = 0;
    while(maxOb < 8) //ligne 2
    {
        for(i=0; i<NB_CASES_L; i++)
        {
            alea = (rand()*(3 - 1)/RAND_MAX) +1;
            if (alea == 1 && t[i][2] != NENUPHAR && maxOb<8)
            {
                to[k] = creerObs(NENUPHAR, i, 2, i*TAILLE_CASE, 2*TAILLE_CASE); k++; maxOb++;
                t[i][2] = NENUPHAR;
            }
        }
    }
    for(i=0; i<NB_CASES_L; i++)
    {
        if(t[i][2] != NENUPHAR)
        {
            to[k] = creerObs(EAU, i, 2, i*TAILLE_CASE, 2*TAILLE_CASE); k++;
            t[i][2] = EAU;
        }
    }
    maxOb = 0;
    while(maxOb < 9) //ligne 3
    {
        for (i=0; i<NB_CASES_L; i++)
        {
            alea = (rand()*(3 - 1)/RAND_MAX) +1;
            if (alea == 1 && t[i][3] != RONDIN && maxOb<9)
            {
                to[k] = creerObs(RONDIN, i, 3, i*TAILLE_CASE, 3*TAILLE_CASE); k++; maxOb++;
                t[i][3] = RONDIN;
            }
        }
    }
    for(i=0; i<NB_CASES_L; i++)
    {
        if(t[i][3] != RONDIN)
        {
            to[k] = creerObs(EAU, i, 3, i*TAILLE_CASE, 3*TAILLE_CASE); k++;
            t[i][3] = EAU;
        }
    }
    maxOb = 0;
    for(j=4; j<NB_CASES_H; j++) //lignes 4-5-6
    {
        for(i=0; i<NB_CASES_L; i++) t[i][j] = SOL;
    }
    while(maxOb < 10) //ligne 7
    {
        for(i=0; i<NB_CASES_L; i++)
        {
            alea = (rand()*(3 - 1)/RAND_MAX) +1;
            if (alea == 1 && t[i][7] != RONDIN && maxOb<10)
            {
                to[k] = creerObs(RONDIN, i, 7, i*TAILLE_CASE, 7*TAILLE_CASE); k++; maxOb++;
                t[i][7] = RONDIN;
            }
        }
    }
    for(i=0; i<NB_CASES_L; i++)
    {
        if(t[i][7] != RONDIN)
        {
            to[k] = creerObs(EAU, i, 7, i*TAILLE_CASE, 7*TAILLE_CASE); k++;
            t[i][7] = EAU;
        }
    }
    maxOb = 0;
    while(maxOb < 7) //ligne 8
    {
        for(i = 0; i<NB_CASES_L; i++)
        {
            alea = (rand()*(3 - 1)/RAND_MAX) +1;
            if (alea == 1 && t[i][8] != NENUPHAR && maxOb<7)
            {
                to[k] = creerObs(NENUPHAR, i, 8, i*TAILLE_CASE, 8*TAILLE_CASE); k++; maxOb++;
                t[i][8] = NENUPHAR;
            }
        }
    }
    for(i=0; i<NB_CASES_L; i++)
    {
        if(t[i][8] != NENUPHAR)
        {
            to[k] = creerObs(EAU, i, 8, i*TAILLE_CASE, 8*TAILLE_CASE); k++;
            t[i][8] = EAU;
        }
    }
    maxOb = 0;
    while(maxOb < 10) //ligne 9
    {
        for(i=0; i<NB_CASES_L; i++)
        {
            alea = (rand()*(3 - 1)/RAND_MAX) +1;
            if (alea == 1 && t[i][9] != RONDIN && maxOb<10)
            {
                to[k] = creerObs(RONDIN, i, 9, i*TAILLE_CASE, 9*TAILLE_CASE); k++; maxOb++;
                t[i][9] = RONDIN;
            }
        }
    }
    for(i=0; i<NB_CASES_L; i++)
    {
        if(t[i][9] != RONDIN)
        {
            to[k] = creerObs(EAU, i, 9, i*TAILLE_CASE, 9*TAILLE_CASE); k++;
            t[i][9] = EAU;
        }
    }
    maxOb = 0;
    while(maxOb < 8) //ligne 10
    {
        for(i = 0; i<NB_CASES_L; i++)
        {
            alea = (rand()*(3 - 1)/RAND_MAX) +1;
            if (alea == 1 && t[i][10] != NENUPHAR && maxOb<8)
            {
                to[k] = creerObs(NENUPHAR, i, 10, i*TAILLE_CASE, 10*TAILLE_CASE); k++; maxOb++;
                t[i][10] = NENUPHAR;
            }
        }
    }
    for(i=0; i<NB_CASES_L; i++)
    {
        if(t[i][10] != NENUPHAR)
        {
            to[k] = creerObs(EAU, i, 10, i*TAILLE_CASE, 10*TAILLE_CASE); k++;
            t[i][10] = EAU;
        }
    }
    maxOb = 0;
    while(maxOb < 10) //ligne 11
    {
        for(i=0; i<NB_CASES_L; i++)
        {
            alea = (rand()*(3 - 1)/RAND_MAX) +1;
            if (alea == 1 && t[i][11] != RONDIN && maxOb<10)
            {
                to[k] = creerObs(RONDIN, i, 11, i*TAILLE_CASE, 11*TAILLE_CASE); k++; maxOb++;
                t[i][11] = RONDIN;
            }
        }
    }
    for(i=0; i<NB_CASES_L; i++)
    {
        if(t[i][11] != RONDIN)
        {
            to[k] = creerObs(EAU, i, 11, i*TAILLE_CASE, 11*TAILLE_CASE); k++;
            t[i][11] = EAU;
        }
    }
    maxOb = 0;
    for(i=0; i<NB_CASES_L; i++) t[i][12] = SOL; //ligne 12
    while(maxOb < 3) //ligne 13
    {
        for(i=0; i<NB_CASES_L; i++)
        {
            alea = (rand()*(10 - 1)/RAND_MAX) +1;
            if (alea == 1 && t[i][13] != VOITUREG && maxOb<3)
            {
                to[k] = creerObs(VOITUREG, i, 13, i*TAILLE_CASE, 13*TAILLE_CASE); k++; maxOb++;
                t[i][13] = VOITUREG;
            }
        }
    }
    maxOb = 0;
    while(maxOb < 3) //ligne 14
    {
        for(i=0; i<NB_CASES_L; i++)
        {
            alea = (rand()*(10 - 1)/RAND_MAX) +1;
            if (alea == 1 && t[i][14] != VOITUREG && maxOb<3)
            {
                to[k] = creerObs(VOITUREG, i, 14, i*TAILLE_CASE, 14*TAILLE_CASE); k++; maxOb++;
                t[i][14] = VOITUREG;
            }
        }
    }
    maxOb = 0;
    while(maxOb < 3) //ligne 15
    {
        for(i=0; i<NB_CASES_L; i++)
        {
            alea = (rand()*(10 - 1)/RAND_MAX) +1;
            if (alea == 1 && t[i][15] != VOITURED && maxOb<3)
            {
                to[k] = creerObs(VOITURED, i, 15, i*TAILLE_CASE, 15*TAILLE_CASE); k++; maxOb++;
                t[i][15] = VOITURED;
            }
        }
    }
    maxOb = 0;
    for(i=0; i<NB_CASES_L; i++) t[i][16] = SOL; //ligne 16
    while(maxOb < 3) //ligne 17
    {
        for(i=0; i<NB_CASES_L; i++)
        {
            alea = (rand()*(10 - 1)/RAND_MAX) +1;
            if (alea == 1 && t[i][17] != VOITURED && maxOb<3)
            {
                to[k] = creerObs(VOITURED, i, 17, i*TAILLE_CASE, 17*TAILLE_CASE); k++; maxOb++;
                t[i][17] = VOITURED;
            }
        }
    }
    maxOb = 0;
    while(maxOb < 3) //ligne 18
    {
        for(i=0; i<NB_CASES_L; i++)
        {
            alea = (rand()*(10 - 1)/RAND_MAX) +1;
            if (alea == 1 && t[i][18] != VOITURED && maxOb<3)
            {
                to[k] = creerObs(VOITURED, i, 18, i*TAILLE_CASE, 18*TAILLE_CASE); k++; maxOb++;
                t[i][18] = VOITURED;
            }
        }
    }
    maxOb = 0;
    while(maxOb < 3) //ligne 19
    {
        for(i=0; i<NB_CASES_L; i++)
        {
            alea = (rand()*(10 - 1)/RAND_MAX) +1;
            if (alea == 1 && t[i][19] != VOITURED && maxOb<3)
            {
                to[k] = creerObs(VOITURED, i, 19, i*TAILLE_CASE, 19*TAILLE_CASE); k++; maxOb++;
                t[i][19] = VOITURED;
            }
        }
    }
    maxOb = 0;
    while(maxOb < 3) //ligne 20
    {
        for(i=0; i<NB_CASES_L; i++)
        {
            alea = (rand()*(10 - 1)/RAND_MAX) +1;
            if (alea == 1 && t[i][20] != VOITUREG && maxOb<3)
            {
                to[k] = creerObs(VOITUREG, i, 20, i*TAILLE_CASE, 20*TAILLE_CASE); k++; maxOb++;
                t[i][20] = VOITUREG;
            }
        }
    }
    maxOb = 0;
    while(maxOb < 3) //ligne 21
    {
        for(i=0; i<NB_CASES_L; i++)
        {
            alea = (rand()*(10 - 1)/RAND_MAX) +1;
            if (alea == 1 && t[i][21] != VOITUREG && maxOb<3)
            {
                to[k] = creerObs(VOITUREG, i, 21, i*TAILLE_CASE, 21*TAILLE_CASE); k++; maxOb++;
                t[i][21] = VOITUREG;
            }
        }
    }
    maxOb = 0;
    for(i=0; i<NB_CASES_L; i++) t[i][22] = SOL; //ligne 22
}

void afficheObstacles(SDL_Surface *ecran)
{
    int i, j, k=0;
    for(i=0; i<NB_CASES_H; i++)
    {
        for(j=0; j<NB_CASES_L; j++)
        {
            for(k=0; k<NB_OBSTACLES; k++)
            {
                if ( (to[k].posx == j ) && (to[k].posy == i) )
                {
                     SDL_BlitSurface(to[k].image, NULL, ecran, &to[k].pospix);
                     k++;
                }
            }
        }
    }
}

/** ACTUALISATIONS  1 JOUEUR**/
void actuGauche(int ligne, SDL_Rect *posJ, Joueur *j) // <---
{
    int k,playerFound=0;    //playerFound permet de d�caler le joueur une seule fois, afin d'�viter qu'il se "t�l�porte" ailleurs sur le rondin
    for (k=0; k<NB_OBSTACLES; k++)
    {
        if (to[k].posy == ligne)
        {
            if(to[k].posx <= 0)
            {
                if(to[k].pospix.x == posJ->x && to[k].pospix.y == posJ->y && to[k].type == RONDIN)
                {
                    j->alive=0;
                }
                to[k].posx = 16;
                to[k].pospix.x = (to[k].posx)*TAILLE_CASE;

            }
            else if (to[k].posx>0)
            {
                if(to[k].pospix.x == posJ->x && to[k].pospix.y == posJ->y && to[k].type == RONDIN && playerFound==0)
                {
                    posJ->x -= TAILLE_CASE;
                    playerFound=1;
                }
                to[k].posx--;
                to[k].pospix.x = (to[k].posx)*TAILLE_CASE;
            }
        }

    }
}

void actuDroite(int ligne, SDL_Rect *posJ, Joueur *j) // --->
{
    int k,playerFound=0;
    for (k=0; k<NB_OBSTACLES; k++)
    {
        if (to[k].posy == ligne)
        {
            if(to[k].posx >= 16)
            {
                if(to[k].pospix.x == posJ->x && to[k].pospix.y == posJ->y && to[k].type == RONDIN)
                {
                    j->alive = 0;
                }
                to[k].posx = 0;
                to[k].pospix.x = (to[k].posx)*TAILLE_CASE;
            }
            else if(to[k].posx < 16)
            {
                if(to[k].pospix.x == posJ->x && to[k].pospix.y == posJ->y && to[k].type == RONDIN && playerFound==0)
                {
                    posJ->x += TAILLE_CASE;
                    playerFound=1;
                }
                to[k].posx++;
                to[k].pospix.x = (to[k].posx)*TAILLE_CASE;
            }
        }
    }
}

void actuTout(SDL_Rect *posJ, Joueur *joueur)
{
    int j;
    for(j=0; j<23; j++)
    {
        if(j==1||j==7||j==11||j==15||j==17||j==18||j==19) actuDroite(j, posJ, joueur);
        if(j==3||j==9||j==13||j==14||j==20||j==21) actuGauche(j, posJ, joueur);
    }
}

/** ACTUALISATIONS  2 JOUEURS**/
void actuGauche2J(int ligne, Joueur *j1, Joueur *j2) // <---
{
    int k,playerFound1=0,playerFound2=0;
    for (k=0; k<NB_OBSTACLES; k++)
    {
        if (to[k].posy == ligne)
        {
            if(to[k].posx <= 0)
            {
                if(to[k].pospix.x == j1->posJ.x && to[k].pospix.y == j1->posJ.y && to[k].type == RONDIN)
                {
                    j1->alive=0;
                }
                if(to[k].pospix.x == j2->posJ.x && to[k].pospix.y == j2->posJ.y && to[k].type == RONDIN)
                {
                    j2->alive=0;
                }
                to[k].posx = 16;
                to[k].pospix.x = (to[k].posx)*TAILLE_CASE;

            }
            else if (to[k].posx>0)
            {
                if(to[k].pospix.x == j1->posJ.x && to[k].pospix.y == j1->posJ.y && to[k].type == RONDIN && playerFound1==0)
                {
                    j1->posJ.x -= TAILLE_CASE;
                    playerFound1=1;
                }
                if(to[k].pospix.x == j2->posJ.x && to[k].pospix.y == j2->posJ.y && to[k].type == RONDIN && playerFound2==0)
                {
                    j2->posJ.x -= TAILLE_CASE;
                    playerFound2=1;
                }
                to[k].posx--;
                to[k].pospix.x = (to[k].posx)*TAILLE_CASE;
            }
        }

    }
}

void actuDroite2J(int ligne, Joueur *j1, Joueur *j2) // --->
{
    int k,playerFound1=0,playerFound2=0;
    for (k=0; k<NB_OBSTACLES; k++)
    {
        if (to[k].posy == ligne)
        {
            if(to[k].posx >= 16)
            {
                if(to[k].pospix.x == j1->posJ.x && to[k].pospix.y == j1->posJ.y && to[k].type == RONDIN)
                {
                    j1->alive=0;
                }
                if(to[k].pospix.x == j2->posJ.x && to[k].pospix.y == j2->posJ.y && to[k].type == RONDIN)
                {
                    j2->alive=0;
                }
                to[k].posx = 0;
                to[k].pospix.x = (to[k].posx)*TAILLE_CASE;
            }
            else if(to[k].posx < 16)
            {
                if(to[k].pospix.x == j1->posJ.x && to[k].pospix.y == j1->posJ.y && to[k].type == RONDIN && playerFound1==0)
                {
                    j1->posJ.x += TAILLE_CASE;
                    playerFound1=1;
                }
                if(to[k].pospix.x == j2->posJ.x && to[k].pospix.y == j2->posJ.y && to[k].type == RONDIN && playerFound2==0)
                {
                    j2->posJ.x += TAILLE_CASE;
                    playerFound2=1;
                }
                to[k].posx++;
                to[k].pospix.x = (to[k].posx)*TAILLE_CASE;
            }
        }
    }
}

void actuTout2J(Joueur *j1, Joueur *j2)
{
    int j;
    for(j=0; j<23; j++)
    {
        if(j==1||j==7||j==11||j==15||j==17||j==18||j==19) actuDroite2J(j, j1, j2);
        if(j==3||j==9||j==13||j==14||j==20||j==21) actuGauche2J(j, j1, j2);
    }
}

/** CHOIX DES SKINS **/
void choixCarte(SDL_Surface **fond)
{
    int s = (rand()*(6 - 1)/RAND_MAX) +1 ;
    switch(s)
    {
        case 1:
            *fond = SDL_LoadBMP("map.bmp"); break;
        case 2:
            *fond = SDL_LoadBMP("map2.bmp"); break;
        case 3:
            *fond = SDL_LoadBMP("map3.bmp"); break;
        case 4:
            *fond = SDL_LoadBMP("map4.bmp"); break;
        case 5:
            *fond = SDL_LoadBMP("map5.bmp"); break;
        default:
            break;
    }
    //return s; //pour definir les sprites d'obstacles
}

void choixSkin(SDL_Surface *player[4])
{
    int s = (rand()*(11 - 1)/RAND_MAX) +1 ;
    switch(s)
    {
        case 1:
            player[GAUCHE] = SDL_LoadBMP("FrogLeft.bmp");
            player[BAS] = SDL_LoadBMP("FrogDown.bmp");
            player[DROITE] = SDL_LoadBMP("FrogRight.bmp");
            player[HAUT] = SDL_LoadBMP("FrogUp.bmp");
            break;
        case 2:
            player[GAUCHE] = SDL_LoadBMP("FrogLeft_blue.bmp");
            player[BAS] = SDL_LoadBMP("FrogDown_blue.bmp");
            player[DROITE] = SDL_LoadBMP("FrogRight_blue.bmp");
            player[HAUT] = SDL_LoadBMP("FrogUp_blue.bmp");
            break;
        case 3:
            player[GAUCHE] = SDL_LoadBMP("FrogLeft_original.bmp");
            player[BAS] = SDL_LoadBMP("FrogDown_original.bmp");
            player[DROITE] = SDL_LoadBMP("FrogRight_original.bmp");
            player[HAUT] = SDL_LoadBMP("FrogUp_original.bmp");
            break;
        case 4:
            player[GAUCHE] = SDL_LoadBMP("FrogLeft_yellow.bmp");
            player[BAS] = SDL_LoadBMP("FrogDown_yellow.bmp");
            player[DROITE] = SDL_LoadBMP("FrogRight_yellow.bmp");
            player[HAUT] = SDL_LoadBMP("FrogUp_yellow.bmp");
            break;
        case 5:
            player[GAUCHE] = SDL_LoadBMP("FrogLeft_oldies.bmp");
            player[BAS] = SDL_LoadBMP("FrogDown_oldies.bmp");
            player[DROITE] = SDL_LoadBMP("FrogRight_oldies.bmp");
            player[HAUT] = SDL_LoadBMP("FrogUp_oldies.bmp");
            break;
        case 6:
            player[GAUCHE] = SDL_LoadBMP("FrogLeft_red.bmp");
            player[BAS] = SDL_LoadBMP("FrogDown_red.bmp");
            player[DROITE] = SDL_LoadBMP("FrogRight_red.bmp");
            player[HAUT] = SDL_LoadBMP("FrogUp_red.bmp");
            break;
        case 7:
            player[GAUCHE] = SDL_LoadBMP("FrogLeft_ninja.bmp");
            player[BAS] = SDL_LoadBMP("FrogDown_ninja.bmp");
            player[DROITE] = SDL_LoadBMP("FrogRight_ninja.bmp");
            player[HAUT] = SDL_LoadBMP("FrogUp_ninja.bmp");
            break;
         case 8:
            player[GAUCHE] = SDL_LoadBMP("FrogLeft_purple.bmp");
            player[BAS] = SDL_LoadBMP("FrogDown_purple.bmp");
            player[DROITE] = SDL_LoadBMP("FrogRight_purple.bmp");
            player[HAUT] = SDL_LoadBMP("FrogUp_purple.bmp");
            break;
         case 9:
            player[GAUCHE] = SDL_LoadBMP("FrogLeft_ghost.bmp");
            player[BAS] = SDL_LoadBMP("FrogDown_ghost.bmp");
            player[DROITE] = SDL_LoadBMP("FrogRight_ghost.bmp");
            player[HAUT] = SDL_LoadBMP("FrogUp_ghost.bmp");
            break;
         case 10:
            player[GAUCHE] = SDL_LoadBMP("FrogLeft_arctic.bmp");
            player[BAS] = SDL_LoadBMP("FrogDown_arctic.bmp");
            player[DROITE] = SDL_LoadBMP("FrogRight_arctic.bmp");
            player[HAUT] = SDL_LoadBMP("FrogUp_arctic.bmp");
            break;
        default:
            break;
    }
}

/** COLLISIONS **/
void collisions(SDL_Rect *posJ, Joueur *j)
{
    int k;
    for(k=0; k<NB_OBSTACLES; k++)
    {
        if (to[k].pospix.x == posJ->x && to[k].pospix.y == posJ->y && (to[k].type == VOITURED || to[k].type == VOITUREG || to[k].type == EAU))
        {
            j->alive = 0;
        }
    }
}

/***********************************/
/***********************************/
/***********************************/

void jouer(SDL_Surface *ecran, int *InGame, int difficultyVar, const SDL_VideoInfo *Resolution,FMOD_SYSTEM *system)
{

    SDL_EventState(SDL_MOUSEMOTION,SDL_DISABLE);

    /** VARIABLES **/
    SDL_Surface *player[4] = {NULL}; //4 surfaces pour 4 directions
    SDL_Surface *playerCurrent; //direction actuelle du joueur
    SDL_Surface *fond;
    SDL_Rect positionJ;
    Joueur j1;
    j1.alive=1;
    j1.numJ=1;
    SDL_Rect posMap; posMap.x=0; posMap.y=0;
    SDL_Event event;
    Tab Tabscores;
    int continuer=1, i=0, k=0, previousTime = 0,cheatVar=0;

    Score newScore;
    newScore.highscore=0;

    GrilleJeu map;

    /** INITIALISATION DU TABLEAU D'OBSTACLES **/
    initTabObsAleatoire(map);

    /** CHARGEMENT DES SPRITES **/
    choixCarte(&fond);
    SDL_BlitSurface(fond, NULL, ecran, &posMap);
    choixSkin(player); /** !! NOUVEAUTE !! **/
    playerCurrent = player[HAUT]; //joueur dirige vers le haut au depart

    /** TRAITEMENT DE LA TRANSPARENCE DES SPRITES **/
    SDL_SetColorKey(player[GAUCHE], SDL_SRCCOLORKEY, SDL_MapRGB(player[GAUCHE]->format, 255, 0, 0));
    SDL_SetColorKey(player[BAS], SDL_SRCCOLORKEY, SDL_MapRGB(player[BAS]->format, 255, 0, 0));
    SDL_SetColorKey(player[DROITE], SDL_SRCCOLORKEY, SDL_MapRGB(player[DROITE]->format, 255, 0, 0));
    SDL_SetColorKey(player[HAUT], SDL_SRCCOLORKEY, SDL_MapRGB(player[HAUT]->format, 255, 0, 0));

    /** ACTIVATION DE LA REPETITION DES TOUCHES **/
    SDL_EnableKeyRepeat(1, 500); //deplace quand on maintient une touche enfoncee

    /** POSITION DE DEPART DU JOUEUR **/
    positionJ.x = 8*TAILLE_CASE;
    positionJ.y = 22*TAILLE_CASE;
    SDL_BlitSurface(playerCurrent, NULL, ecran, &positionJ);

    /** INITIALISATION DE L'ECRAN **/
    SDL_Flip(ecran);

    /** EVENEMENTS **/
    while (continuer)
    {
        SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 153,217,234));
        SDL_BlitSurface(fond, NULL, ecran, &posMap);
        afficheObstacles(ecran);
        SDL_BlitSurface(playerCurrent, NULL, ecran, &positionJ);
        SDL_Flip(ecran);
        collisions(&positionJ, &j1);
        getPlayerState1Joueur(j1, &positionJ, ecran, posMap, &continuer,Resolution);
        if(SDL_GetTicks()-previousTime>400-difficultyVar){
            previousTime=SDL_GetTicks();
            actuTout(&positionJ, &j1);
        }
        while(SDL_PollEvent(&event)){
            switch(event.type)
            {
                case SDL_QUIT:
                    continuer = 0;
                    *InGame=0;
                break;
                case SDL_KEYDOWN:
                    switch(event.key.keysym.sym)
                    {
                        case SDLK_p:
                            setMusic(1,1,system);
                            break;
                        case SDLK_ESCAPE:
                            continuer = 0;
                            SDL_WaitEvent(&event);
                            break;
                        case SDLK_LEFT:
                            playerCurrent = player[GAUCHE];
                            deplacerJ(&positionJ, GAUCHE);
                            break;
                        case SDLK_DOWN:
                            playerCurrent = player[BAS];
                            deplacerJ(&positionJ, BAS);
                            cheatVar-=1;
                            break;
                        case SDLK_RIGHT:
                            playerCurrent = player[DROITE];
                            deplacerJ(&positionJ, DROITE);
                            break;
                        case SDLK_UP:
                            playerCurrent = player[HAUT];
                            deplacerJ(&positionJ, HAUT);
                            if(cheatVar>=0){    //si le joueur ne vient pas de descendre
                                newScore.highscore+=1;   //on incr�mente son score
                                cheatVar=0;     //la variable de triche est remise � z�ro
                            }
                            else{
                                cheatVar+=1;
                            }
                            break;
                        default:
                            break;
                    }
                break;
            }
        }
    }

    /** EFFACEMENT DE L'ECRAN **/
    SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 0, 0, 0));

    /** GESTION DES SCORES **/
    if(*InGame!=0){     //si le joueur n'a pas quitt� le programme
        getHighscoreFromFile(Tabscores);
        if(Tabscores[9].highscore<newScore.highscore){   //si le score le plus faible est inferieur au nouveau, on a un highscore
            newHighscore(ecran,posMap,InGame,newScore,Tabscores);
        }
    }

    /** DESACTIVATION DE LA REPETITION DES TOUCHES **/
    SDL_EnableKeyRepeat(0,0);

    /** LIBERATION DES SURFACES **/
    for(i=0; i<4; i++)
    {
        SDL_FreeSurface(player[i]);
    }
    SDL_FreeSurface(playerCurrent);
    SDL_FreeSurface(fond);
    for(k=0; k<NB_OBSTACLES; k++)
    {
        SDL_FreeSurface(to[k].image);
        memset(&to[k],0,sizeof(Obstacle));
    }
}

/***********************************/
/***********************************/
/***********************************/

void jouer2(SDL_Surface *ecran, int *InGame, const SDL_VideoInfo *Resolution,FMOD_SYSTEM *system)
{

    SDL_EventState(SDL_MOUSEMOTION,SDL_DISABLE);

    /** VARIABLES **/
    SDL_Surface *player1[4] = {NULL}; //4 surfaces pour 4 directions
    SDL_Surface *player2[4] = {NULL};
    SDL_Surface *playerCurrent1, *playerCurrent2; //direction actuelle du joueur
    SDL_Surface *fond;
    Joueur J1,J2;

    J1.alive=1;
    J1.numJ=1;

    J2.alive=1;
    J2.numJ=2;

    SDL_Rect posMap; posMap.x=0; posMap.y=0;
    SDL_Event event;
    int continuer=1, i=0, k=0, previousTime = 0;
    GrilleJeu map;

    /** INITIALISATION DU TABLEAU D'OBSTACLES **/
    initTabObsAleatoire(map);

    /** CHARGEMENT DES SPRITES **/
    choixCarte(&fond);
    SDL_BlitSurface(fond, NULL, ecran, &posMap);
    choixSkin(player1);
    playerCurrent1 = player1[HAUT]; //joueur 1 dirige vers le haut au depart
    choixSkin(player2);
    playerCurrent2 = player2[HAUT]; //joueur 2 dirige vers le haut au depart

    /** TRAITEMENT DE LA TRANSPARENCE DES SPRITES **/
    SDL_SetColorKey(player1[GAUCHE], SDL_SRCCOLORKEY, SDL_MapRGB(player1[GAUCHE]->format, 255, 0, 0));
    SDL_SetColorKey(player1[BAS], SDL_SRCCOLORKEY, SDL_MapRGB(player1[BAS]->format, 255, 0, 0));
    SDL_SetColorKey(player1[DROITE], SDL_SRCCOLORKEY, SDL_MapRGB(player1[DROITE]->format, 255, 0, 0));
    SDL_SetColorKey(player1[HAUT], SDL_SRCCOLORKEY, SDL_MapRGB(player1[HAUT]->format, 255, 0, 0));

    SDL_SetColorKey(player2[GAUCHE], SDL_SRCCOLORKEY, SDL_MapRGB(player2[GAUCHE]->format, 255, 0, 0));
    SDL_SetColorKey(player2[BAS], SDL_SRCCOLORKEY, SDL_MapRGB(player2[BAS]->format, 255, 0, 0));
    SDL_SetColorKey(player2[DROITE], SDL_SRCCOLORKEY, SDL_MapRGB(player2[DROITE]->format, 255, 0, 0));
    SDL_SetColorKey(player2[HAUT], SDL_SRCCOLORKEY, SDL_MapRGB(player2[HAUT]->format, 255, 0, 0));

    /** POSITION DE DEPART DES JOUEURS **/
    J1.posJ.x = 4*TAILLE_CASE;
    J1.posJ.y = 22*TAILLE_CASE;

    J2.posJ.x = 12*TAILLE_CASE;
    J2.posJ.y = 22*TAILLE_CASE;

    SDL_BlitSurface(playerCurrent1, NULL, ecran, &J1.posJ);
    SDL_BlitSurface(playerCurrent2, NULL, ecran, &J2.posJ);

    /** INITIALISATION DE L'ECRAN **/
    SDL_Flip(ecran);

    /** EVENEMENTS **/
    while (continuer)
    {
        SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 153,217,234));
        SDL_BlitSurface(fond, NULL, ecran, &posMap);
        afficheObstacles(ecran);
        SDL_BlitSurface(playerCurrent1, NULL, ecran, &J1.posJ);
        SDL_BlitSurface(playerCurrent2, NULL, ecran, &J2.posJ);
        SDL_Flip(ecran);
        collisions(&J1.posJ, &J1);
        collisions(&J2.posJ, &J2);
        getPlayerState2Joueurs(&J1, ecran, &continuer, Resolution);
        getPlayerState2Joueurs(&J2, ecran, &continuer, Resolution);
        if(SDL_GetTicks()-previousTime>300){    //pas de mode de difficult� ici / 400ms
            previousTime=SDL_GetTicks();
            actuTout2J(&J1, &J2);
        }
        while(SDL_PollEvent(&event)){
            switch(event.type)
            {
                case SDL_QUIT:
                    continuer = 0;
                    *InGame=0;
                break;
                case SDL_KEYDOWN:
                    switch(event.key.keysym.sym)
                    {
                        case SDLK_p:
                            setMusic(1,1,system);
                            break;
                        case SDLK_ESCAPE:
                            continuer = 0;
                            SDL_WaitEvent(&event);
                            break;
                        case SDLK_LEFT:
                            playerCurrent2 = player2[GAUCHE];
                            deplacerJ(&J2.posJ, GAUCHE);
                            break;
                        case SDLK_DOWN:
                            playerCurrent2 = player2[BAS];
                            deplacerJ(&J2.posJ, BAS);
                            break;
                        case SDLK_RIGHT:
                            playerCurrent2 = player2[DROITE];
                            deplacerJ(&J2.posJ, DROITE);
                            break;
                        case SDLK_UP:
                            playerCurrent2 = player2[HAUT];
                            deplacerJ(&J2.posJ, HAUT);
                            break;
                        case SDLK_w:
                            playerCurrent1 = player1[HAUT];
                            deplacerJ(&J1.posJ,HAUT);
                            break;
                        case SDLK_s:
                            playerCurrent1 = player1[BAS];
                            deplacerJ(&J1.posJ,BAS);
                            break;
                        case SDLK_a:
                            playerCurrent1 = player1[GAUCHE];
                            deplacerJ(&J1.posJ,GAUCHE);
                            break;
                        case SDLK_d:
                            playerCurrent1 = player1[DROITE];
                            deplacerJ(&J1.posJ,DROITE);
                            break;
                        default:
                            break;
                    }
                break;
            }
        }
    }

    /** EFFACEMENT DE L'ECRAN **/
    SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 0, 0, 0));

    /** DESACTIVATION DE LA REPETITION DES TOUCHES **/
    SDL_EnableKeyRepeat(0,0);

    /** LIBERATION DES SURFACES **/
    for(i=0; i<4; i++)
    {
        SDL_FreeSurface(player1[i]);
        SDL_FreeSurface(player2[i]);
    }
    SDL_FreeSurface(playerCurrent1);
    SDL_FreeSurface(playerCurrent2);
    SDL_FreeSurface(fond);
    for(k=0; k<NB_OBSTACLES; k++)
    {
        SDL_FreeSurface(to[k].image);
        memset(&to[k],0,sizeof(Obstacle));  //le tableau peut �tre r�utilis� lors d'une autre partie
    }
}
