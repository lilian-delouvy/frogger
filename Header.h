#ifndef HEADER1_H_INCLUDED
#define HEADER1_H_INCLUDED
#include <SDL/SDL.h>
#include <FMOD/fmod.h>

#define TAILLE_CASE 32 //tailles des cellules : 32px
#define NB_CASES_L 17 //17 cases/colonne
#define NB_CASES_H 23 //23 cases/ligne
#define L_FENETRE (TAILLE_CASE*NB_CASES_L)
#define H_FENETRE (TAILLE_CASE*NB_CASES_H)
/** **/
#define NB_OBSTACLES 160 //160

enum {GAUCHE, BAS, DROITE, HAUT};
enum {SOL, JOUEUR, EAU, RONDIN, NENUPHAR, VOITUREG,VOITURED}; //0, 1, 2, 3, 4, 5 /**MODIFIE SCE 5**/

typedef int GrilleJeu[NB_CASES_L][NB_CASES_H];

typedef struct{
    char *nomjoueur;
    int highscore;
}Score;

/*********************************************************/
typedef struct
{
    int numJ;
    int posx; int posy;
    int posi; int posj;
    int alive;
    SDL_Rect posJ;
}Joueur;

typedef struct
{
    int type; //RONDIN=3; NENUPHAR=4; VOITURE=5;
    int posx; int posy; //positions x et y dans la matrice GrilleJeu
    SDL_Rect pospix; //positions x et y en pixels
    SDL_Surface *image;
}Obstacle;

typedef Obstacle TabObs[NB_OBSTACLES];
extern TabObs to;
/*********************************************************/

typedef Score Tab[10];  //On ne garde que 10 scores (les 10 meilleurs)

/** DEFINITION DES FONCTIONS **/

/** Jeu **/

void jouer(SDL_Surface *screen,int *InGame, int difficultyVar, const SDL_VideoInfo *Resolution,FMOD_SYSTEM *system);
void jouer2(SDL_Surface *ecran, int *InGame,const SDL_VideoInfo *Resolution,FMOD_SYSTEM *system);

/** Parametres **/

void appelParametres(SDL_Surface *screen, SDL_Rect posMenu, int *InGame,const SDL_VideoInfo *Resolution,FMOD_SYSTEM *system);
void appelButDuJeu(SDL_Surface *screen, SDL_Rect posMenu, int *InGame,FMOD_SYSTEM *system);
void appelCredits(SDL_Surface *screen, SDL_Rect posMenu, int *InGame,FMOD_SYSTEM *system);
void appelScores(SDL_Surface *screen,SDL_Rect posMenu,int *InGame, const SDL_VideoInfo *Resolution,FMOD_SYSTEM *system);
void newHighscore(SDL_Surface *screen, SDL_Rect posMenu, int *InGame,Score newScore,Tab tabscores);
int appelDifficulty(SDL_Surface *screen, SDL_Rect posMenu, int *InGame,FMOD_SYSTEM *system);

/** Highscore **/

void Initialisation(Tab a);
void Triscores(Tab a,Score newscore);
void Ecriture(Tab a);
void LectureScores();
void getHighscoreFromFile(Tab getscores);

/** Music **/

FMOD_SYSTEM *setMusic(int setValue, int musicState, FMOD_SYSTEM *system);

#endif // HEADER1_H_INCLUDED
