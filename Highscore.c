#include <stdio.h>
#include <stdlib.h>
#include "Header.h"
#include <string.h>

//Highscores utiles uniquement pour le mode 1 joueur, puisque le mode 2 joueurs est une simple competition

void Initialisation(Tab a){
    for(int i=0;i<10;i++){
        a[i].highscore=0;
        a[i].nomjoueur="N/A";
    }
}

void Triscores(Tab a,Score newscore){   //On n'a besoin de trier le tableau que quand il y a un nouveau score en entr�e: on a donc 11 Scores a tester
    if(a[9].highscore<newscore.highscore){  //Pas besoin d'aller plus loin dans le tri si le nouveau score est inf�rieur au score le plus bas du tableau
        if(a[8].highscore>newscore.highscore){  //On regarde pour la case du tableau au score le plus faible
            a[9].highscore=newscore.highscore;
            a[9].nomjoueur=newscore.nomjoueur;
        }
        else{
            for(int k=8;k>=0;k--){              //On commence par les scores les plus faibles(sauf le plus bas pour �viter une it�ration inutile dans la boucle pour)
                if(a[k].highscore<newscore.highscore){
                    a[k+1]=a[k];
                    a[k]=newscore;
                }
                else
                    break;      //d�s que le score du tableau est trop important, il est inutile de continuer � comparer
            }
        }
    }
}

void Ecriture(Tab a){
    FILE *f;
    f=fopen("FichierScores.txt","w");
    if(f!=NULL){
        for(int i=0;i<10;i++){
            fprintf(f,"Joueur: %3s Score: %d",a[i].nomjoueur,a[i].highscore);
            fprintf(f,"\n");
        }
        fclose(f);
    }
}

void LectureScores(){
    FILE *f;
    f=fopen("FichierScores.txt","r");
    if(f!=NULL){
        char line[32];
        while(fgets(line,sizeof line,f)!=NULL)  //On verifie que la lecture de ligne s'est bien pass�e
            printf("%s",line);
        fclose(f);
    }
    else
        printf("Lecture des scores impossible\n");
}

void getHighscoreFromFile(Tab getscores){
    FILE *f;
    f=fopen("FichierScores.txt","r");
    if(f!=NULL){
        char line[32];
        int i=0;
        while (fgets(line, 32 ,f)!=NULL && (i<10)) {
                getscores[i].nomjoueur = calloc(3, sizeof(char));
                sscanf(line,"Joueur: %3s Score: %d", getscores[i].nomjoueur, &(getscores[i].highscore));
                i++;
        }
        fclose(f);
    }
    else{   //on est dans une situation o� le fichier n'existe pas
        Initialisation(getscores);  //on initialise et on cr�e le fichier
        Ecriture(getscores);
    }
}
