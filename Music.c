#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>
#include <fmod/fmod.h>

FMOD_SYSTEM *setMusic(int setValue, int musicState, FMOD_SYSTEM *system){

    /** TRAITEMENT DE LA MISE EN PAUSE **/

    if(musicState!=0){  //on veut modifier l'�tat de la musique (pause ou marche)
       FMOD_CHANNELGROUP *channel;
       FMOD_System_GetMasterChannelGroup(system,&channel);
       FMOD_BOOL state;
       FMOD_ChannelGroup_GetPaused(channel,&state);
       if(state){   //si pause
            FMOD_ChannelGroup_SetPaused(channel,0); //on relance la musique
       }
       else{
            FMOD_ChannelGroup_SetPaused(channel,1); //on met en pause
       }
    }

    /** TRAITEMENT DE LA MUSIQUE **/

    else{
        FMOD_SOUND *sound;
        FMOD_RESULT result;
        result=FMOD_System_CreateSound(system,"Waterflame-Hexagon_Force.mp3", FMOD_SOFTWARE | FMOD_2D | FMOD_CREATESTREAM | FMOD_LOOP_NORMAL,0,&sound);

        if(result!=FMOD_OK){     //on v�rifie que la musique a bien �t� r�cup�r�e
            fprintf(stderr,"Ouverture du fichier mp3 impossible \n");
            exit(EXIT_FAILURE);
        }
        //puisqu'on est dans un jeu, on doit activer la r�p�tition de la musique � l'infini, sinon au bout de quelques minutes il n'y aurait plus de musique
        FMOD_Sound_SetLoopCount(sound,(-1));

        FMOD_System_PlaySound(system,FMOD_CHANNEL_FREE,sound,0,NULL);
        if(setValue==0){    //on veut couper compl�tement la musique
            FMOD_Sound_Release(sound);
            FMOD_System_Close(system);
            FMOD_System_Release(system);
        }
    }
    return system;
}
