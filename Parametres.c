#include<stdlib.h>
#include<stdlib.h>
#include<SDL.h>
#include"Header.h"
#include <SDL/SDL_ttf.h>

void SetFullscreen(SDL_Surface *screen, const SDL_VideoInfo *Resolution){
    screen=SDL_SetVideoMode(Resolution->current_w,Resolution->current_h,32,SDL_HWSURFACE | SDL_DOUBLEBUF | SDL_FULLSCREEN);
}

void UnsetFullscreen(SDL_Surface *screen, const SDL_VideoInfo *Resolution){
    screen=SDL_SetVideoMode(Resolution->current_w,Resolution->current_h,32,SDL_HWSURFACE | SDL_DOUBLEBUF | SDL_RESIZABLE);
}

void newHighscore(SDL_Surface *screen, SDL_Rect posMenu, int *InGame,Score newScore,Tab tabscores){    //puisque l'on a besoin de p pour entrer le score, on ne peut pas modifier l'�tat de la musique

    SDL_EventState(SDL_ACTIVEEVENT,SDL_DISABLE);    //sortir de la fen�tre provoquait un affichage de "caract�re sp�cial"
    SDL_EnableKeyRepeat(0, 0);  //Pour �viter d'afficher le message plus d'une fois si le joueur reste appuy�

    TTF_Init();
    SDL_Event event;
    int continuer=1,varError=0,j; //varError nous permet de verifier si l'utilisateur a d�j� entr� autre chose qu'une lettre par la suite
    char chaine[4];     //4 pour que la derni�re case soit '\0'
    for(j=0;j<4;j++)    //on initialise pour �viter que l'affichage de textInput donne un faux message
        chaine[j]='\0';
    TTF_Font *police=TTF_OpenFont("disposabledroid-bb.regular.ttf",40);
    SDL_Color couleur={82,217,43};  //Vert fonc�
    SDL_Surface *texte=TTF_RenderText_Blended(police,"Vous avez un nouveau score ! Entrez un nom: (Maximum de 3 caracteres)",couleur);
    char *chaineScore=NULL;
    chaineScore=calloc(1,sizeof(int));
    sprintf(chaineScore,"%d",newScore.highscore);
    SDL_Surface *texteScore=TTF_RenderText_Blended(police,chaineScore,couleur);
    SDL_Surface *textInput=NULL;
    SDL_Rect posTextInput;
    SDL_FillRect(screen,NULL,SDL_MapRGB(screen->format,153,217,234));
    SDL_BlitSurface(texte,NULL,screen,&posMenu);
    posMenu.y+=35;
    posTextInput.x=posMenu.x;
    posTextInput.y=posMenu.y+35;
    SDL_BlitSurface(texteScore,NULL,screen,&posMenu);
    SDL_Flip(screen);

    /** GESTION DU TEXT INPUT **/

    SDL_EnableUNICODE(SDL_ENABLE);
    SDL_EventState(SDL_MOUSEMOTION,SDL_DISABLE);
    while(continuer<4){ //on veut 3 caract�res et pour entrer dans le while, i est d�j� initialis� � 1
        SDL_WaitEvent(&event);
        if(event.type == SDL_QUIT){
            *InGame=0;
            newScore.highscore=-1;
            break;
        }
        if(event.type == SDL_KEYDOWN){
            if(event.key.keysym.sym == SDLK_ESCAPE){ //l'utilisateur ne souhaite pas entrer de score
                SDL_WaitEvent(&event);  //on le laisse rel�cher sa touche pour �viter des interf�rences avec le KEYUP du main (ligne 57)
                newScore.highscore=-1; //on fait en sorte que le score ne soit jamais entr� dans le tableau des scores
                break;
            }
            if(event.key.keysym.unicode>='a' && event.key.keysym.unicode<='z'){
                chaine[continuer-1]=event.key.keysym.unicode;
                continuer++;
                SDL_WaitEvent(&event);  //on permet au joueur de rel�cher la touche
            }
            else if(event.key.keysym.unicode >='A' && event.key.keysym.unicode<='Z'){
                chaine[continuer-1]=event.key.keysym.unicode;
                continuer++;
                SDL_WaitEvent(&event);
            }
            else{
                texte=TTF_RenderText_Blended(police,"Le caractere entre est un caractere special ! Veuillez entrer uniquement des lettres",couleur);
                if(varError == 0){  //si l'utilisateur n'a jamais entr� de caract�res "non consid�r�s" auparavant, on d�cale le texte pour �viter d'�crire par-dessus
                    posMenu.x+=35;
                    varError = 1;
                }   //une fois cela fait, plus besoin de le faire
                posMenu.y+=35;  //on d�cale quand m�me suivant y au cas o� il y aurait d'autres erreurs
                SDL_BlitSurface(texte,NULL,screen,&posMenu);
                SDL_Flip(screen);
                SDL_WaitEvent(&event);
            }
        }
        textInput=TTF_RenderText_Blended(police,chaine,couleur);
        SDL_BlitSurface(textInput,NULL,screen,&posTextInput);
        SDL_Flip(screen);
    }

    /** LIBERATION DE LA MEMOIRE ET MODIFICATION DU NOUVEAU SCORE **/

    SDL_EnableUNICODE(SDL_DISABLE);
    SDL_FreeSurface(texte);
    SDL_FreeSurface(textInput);
    TTF_CloseFont(police);
    TTF_Quit();
    newScore.nomjoueur=chaine;

    /** TRI ET ECRITURE **/
    Triscores(tabscores,newScore);
    Ecriture(tabscores);
}

void appelScores(SDL_Surface *screen,SDL_Rect posMenu,int *InGame, const SDL_VideoInfo *Resolution,FMOD_SYSTEM *system){
    int i,continuer=1;
    TTF_Init();
    SDL_Event Event;
    TTF_Font *police = TTF_OpenFont("disposabledroid-bb.regular.ttf",50);
    SDL_Color couleur={82,217,43};  //Vert fonc�
    SDL_Surface *texte=TTF_RenderText_Blended(police,"Tableau des scores :",couleur);
    posMenu.x=((Resolution->current_w)/2-(385/2));  //385 car on op�re dix d�calages de 35 en plus de la position initiale (35*10 +35 = 385)
    posMenu.y=((Resolution->current_h)/2-(385/2));
    SDL_FillRect(screen,NULL,SDL_MapRGB(screen->format,153,217,234));
    SDL_BlitSurface(texte,NULL,screen,&posMenu);

    //Ecriture des differents scores:

    Tab scoreboard;
    getHighscoreFromFile(scoreboard);
    char *chaine=NULL;
    for(i=0;i<10;i++){      //il y a dix scores stock�s
        chaine=realloc(chaine,19*sizeof(char));      //on utilise la chaine pour remplir la surface texte. 19 car 3 caract�res, 3 chiffres, 15 espaces
        posMenu.y+=35;
        sprintf(chaine, "%3s",scoreboard[i].nomjoueur);
        texte=TTF_RenderText_Blended(police,chaine,couleur);
        SDL_BlitSurface(texte,NULL,screen,&posMenu);
        posMenu.x+=350;
        sprintf(chaine,"%3d",scoreboard[i].highscore);
        texte=TTF_RenderText_Blended(police,chaine,couleur);
        SDL_BlitSurface(texte,NULL,screen,&posMenu);
        posMenu.x-=350;
    }

    SDL_Flip(screen);

    while(continuer){
        if(SDL_WaitEvent(&Event)){
            switch(Event.type){
                case SDL_QUIT:
                    *InGame = 0;
                    continuer=0;
                break;
                case SDL_KEYUP:
                    switch(Event.key.keysym.sym){
                        case SDLK_p:
                            setMusic(1,1,system);
                        break;
                        default:
                            continuer=0;
                            break;
                    }
                break;
                default:
                    break;
            }
        }
    }
    SDL_FreeSurface(texte);
    TTF_CloseFont(police);
    TTF_Quit();
}

void appelCredits(SDL_Surface *screen,SDL_Rect posMenu,int *InGame,FMOD_SYSTEM *system){

    /** DECLARATION DES VARIABLES **/

    SDL_Surface *credits=SDL_LoadBMP("Credits.bmp");        //Ne SURTOUT PAS mettre d'accent � "Cr�dits"
    SDL_Event Event;
    int continuer=1;

    SDL_FillRect(screen,NULL,SDL_MapRGB(screen->format,153,217,234));
    SDL_BlitSurface(credits,NULL,screen,&posMenu);      //rien n'empeche d'utiliser les coordonnees du menu plutot que d'en definir de nouvelles pour les credits!
    SDL_Flip(screen);

    while(continuer){
        if(SDL_WaitEvent(&Event)){  //On v�rifie qu'on a bien re�u un �v�nement
            switch(Event.type){     //On doit traiter le cas o� l'utilisateur veut quitter � chaque instant
                case SDL_QUIT:
                    *InGame = 0;
                    continuer=0;
                break;
                case SDL_KEYUP:
                    switch(Event.key.keysym.sym){
                    case SDLK_p:
                        setMusic(1,1,system);
                    break;
                    default:
                        continuer=0;
                        break;
                    }
                break;
                default:
                    break;
            }
        }
    }
    SDL_FreeSurface(credits);
}

void appelButDuJeu(SDL_Surface *screen, SDL_Rect posMenu, int *InGame,FMOD_SYSTEM *system){

    /** DECLARATION DES VARIABLES **/

    SDL_Surface *regles=SDL_LoadBMP("Regles.bmp");
    SDL_Event Event;
    int continuer=1;

    SDL_FillRect(screen,NULL,SDL_MapRGB(screen->format,153,217,234));
    SDL_BlitSurface(regles,NULL,screen,&posMenu);
    SDL_Flip(screen);
    while(continuer){
        SDL_WaitEvent(&Event);
        switch(Event.type){
            case SDL_QUIT:
                *InGame = 0;
                continuer=0;
            break;
            case SDL_KEYUP:
                switch(Event.key.keysym.sym){
                    case SDLK_p:
                        setMusic(1,1,system);
                    break;
                    default:
                        continuer=0;
                        break;
                }
            break;
            default:
                break;
        }
    }
    SDL_FreeSurface(regles);
}

int appelDifficulty(SDL_Surface *screen, SDL_Rect posMenu, int *InGame,FMOD_SYSTEM *system){
    int difficultyVar=0;    //1 pour le normal, 2 pour le difficile
    SDL_Surface *difficultyMenu=SDL_LoadBMP("Difficulty.bmp");
    SDL_Event Event;
    int continuer=1;
    while(continuer){
        SDL_FillRect(screen,NULL,SDL_MapRGB(screen->format,153,217,234));
        SDL_BlitSurface(difficultyMenu,NULL,screen,&posMenu);
        SDL_Flip(screen);
        SDL_WaitEvent(&Event);
        switch(Event.type){
        case SDL_QUIT:
            continuer=0;
            *InGame=0;
        break;
        case SDL_KEYUP:
            switch(Event.key.keysym.sym){
            case SDLK_KP1:
                difficultyVar=1;
                continuer=0;
            break;
            case SDLK_KP2:
                difficultyVar=2;
                continuer=0;
            break;
            case SDLK_p:
                setMusic(1,1,system);
            break;
            default:
                continuer=0;
                break;
            }
        break;
        case SDL_MOUSEBUTTONDOWN:
            if(Event.button.button == SDL_BUTTON_LEFT && Event.button.x>(posMenu.x+445) && Event.button.x<(posMenu.x+565) && Event.button.y>(275+posMenu.y) && Event.button.y<(posMenu.y+310)){
                difficultyVar=1;
                continuer=0;
            }
            if(Event.button.button == SDL_BUTTON_LEFT && Event.button.x>(posMenu.x+445) && Event.button.x<(posMenu.x+630) && Event.button.y>(425+posMenu.y) && Event.button.y<(posMenu.y+455)){
                difficultyVar=2;
                continuer=0;
            }
        break;
        default:
            break;
        }
    }
    SDL_FreeSurface(difficultyMenu);
    return difficultyVar;
}

void appelControles(SDL_Surface *screen, SDL_Rect posMenu, int *InGame,FMOD_SYSTEM *system){

    SDL_Surface *controles=SDL_LoadBMP("Controles.bmp");
    SDL_Event Event;
    int continuer=1;

    while(continuer){
        SDL_FillRect(screen,NULL,SDL_MapRGB(screen->format,153,217,234));
        SDL_BlitSurface(controles,NULL,screen,&posMenu);
        SDL_Flip(screen);
        SDL_WaitEvent(&Event);
        switch(Event.type){
        case SDL_QUIT:
            continuer=0;
            *InGame=0;
        break;
        case SDL_KEYUP:
            switch(Event.key.keysym.sym){
            case SDLK_p:
                setMusic(1,1,system);
                break;
            default:
                continuer=0;
                break;
            }
        break;
            default:
                break;
        }
    }
    SDL_FreeSurface(controles);
}

void appelParametres(SDL_Surface *screen, SDL_Rect posMenu, int *InGame, const SDL_VideoInfo *Resolution,FMOD_SYSTEM *system){

    /** DECLARATION DES VARIABLES **/

    SDL_Surface *parametres = SDL_LoadBMP("Parametres.bmp");
    SDL_Event Event;
    int continuer=1;

    while(continuer){
        SDL_FillRect(screen,NULL,SDL_MapRGB(screen->format,153,217,234));
        SDL_BlitSurface(parametres,NULL,screen,&posMenu);
        SDL_Flip(screen);
        SDL_WaitEvent(&Event);
            switch(Event.type){
            case SDL_QUIT:
                continuer=0;
                *InGame=0;
            break;
            case SDL_KEYUP:
                switch(Event.key.keysym.sym){
                case SDLK_KP1:
                    SetFullscreen(screen,Resolution);
                break;
                case SDLK_KP2:
                    UnsetFullscreen(screen,Resolution);
                break;
                case SDLK_KP3:
                    appelControles(screen,posMenu,InGame,system);
                    if(*InGame==0){
                        continuer=0;
                    }
                break;
                case SDLK_p:
                    setMusic(1,1,system);
                break;
                default:
                    continuer=0;
                    break;
                }
            break;
            case SDL_MOUSEBUTTONDOWN:
                //Activation du fullscreen
                if(Event.button.button == SDL_BUTTON_LEFT && Event.button.x>(390+posMenu.x) && Event.button.x<(650+posMenu.x) && Event.button.y>(250+posMenu.y) && Event.button.y<(275+posMenu.y)){
                    SetFullscreen(screen,Resolution);
                }
                //Desactivation du fullscreen
                if(Event.button.button == SDL_BUTTON_LEFT && Event.button.x>(455+posMenu.x) && Event.button.x<(575+posMenu.x) && Event.button.y>(340+posMenu.y) && Event.button.y<(370+posMenu.y)){
                    UnsetFullscreen(screen,Resolution);
                }
                if(Event.button.button == SDL_BUTTON_LEFT && Event.button.x>(440+posMenu.x) && Event.button.x<(590+posMenu.x) && Event.button.y>(430+posMenu.y) && Event.button.y<(455+posMenu.y)){
                    appelControles(screen,posMenu,InGame,system);
                    if(*InGame==0){     //on peut tr�s bien vouloir quitter le jeu depuis les contr�les
                        continuer=0;
                    }
                }
            break;
            default:
                break;
            }
        }
        SDL_FreeSurface(parametres);
    }
