#include <stdlib.h>
#include <SDL/SDL.h>
#include "Header.h"
#include <SDL/SDL_ttf.h>
#include <fmod/fmod.h>

extern void jouer(SDL_Surface *screen,int *InGame, int difficultyVar,const SDL_VideoInfo *Resolution,FMOD_SYSTEM *system);
extern void jouer2(SDL_Surface *ecran, int *InGame,const SDL_VideoInfo *Resolution,FMOD_SYSTEM *system);
extern void appelScores(SDL_Surface *screen,SDL_Rect posMenu,int *InGame, const SDL_VideoInfo *Resolution,FMOD_SYSTEM *system);
extern void newHighscore(SDL_Surface *screen, SDL_Rect posMenu, int *InGame,Score newScore,Tab tabscores);
extern void appelParametres(SDL_Surface *screen, SDL_Rect posMenu, int *InGame,const SDL_VideoInfo *Resolution,FMOD_SYSTEM *system);
extern void appelButDuJeu(SDL_Surface *screen, SDL_Rect posMenu, int *InGame,FMOD_SYSTEM *system);
extern void appelCredits(SDL_Surface *screen, SDL_Rect posMenu, int *InGame,FMOD_SYSTEM *system);
extern int appelDifficulty(SDL_Surface *screen, SDL_Rect posMenu, int *InGame,FMOD_SYSTEM *system);
extern FMOD_SYSTEM *setMusic(int setValue, int musicState, FMOD_SYSTEM *system);

int main(int argc, char *argv[]){

    /**DECLARATION DES VARIABLES**/
    SDL_Surface *screen= NULL , *menu=NULL;

    SDL_Rect posMenu;

    SDL_Event Event;
    const SDL_VideoInfo *Resolution;

    int InGame=1,chosenDifficulty=0;

    FMOD_SYSTEM *system;

    /**INITIALISATION**/

    FMOD_System_Create(&system);
    FMOD_System_Init(system,1,FMOD_INIT_NORMAL,NULL);

    SDL_EventState(SDL_MOUSEMOTION,SDL_DISABLE);        //on d�sactive la prise en consid�ration des mouvements de souris
    SDL_Init(SDL_INIT_VIDEO);
    TTF_Init();
    Resolution=SDL_GetVideoInfo();      //On r�cup�re les infos sur la r�solution de l'�cran du pc. Il est n�cessaire d'avoir initialis� la SDL avant
    SDL_WM_SetIcon(SDL_LoadBMP("GameIcon.bmp"),NULL);
    screen = SDL_SetVideoMode(0,0,32,SDL_HWSURFACE | SDL_DOUBLEBUF | SDL_RESIZABLE);
    posMenu.x=((Resolution->current_w)/2-(1024/2));  //calculs pour centrer le menu
    //1024 et 575 sont les dimensions de l'image du menu (peuvent �tre trouv�es dans Propri�t�s --> D�tails de l'image)
    posMenu.y=((Resolution->current_h)/2-(575/2));
    if(Resolution->current_h<575){
        //Resolution trop faible pour permettre un affichage
        return EXIT_FAILURE;
    }
    SDL_WM_SetCaption("Frogger Remastered",NULL);
    menu = SDL_LoadBMP("StartMenu.bmp");
    SDL_WM_ToggleFullScreen(screen);
    setMusic(1,0,system);
    /**DEBUT DU JEU**/

    while(InGame){
        SDL_FillRect(screen,NULL,SDL_MapRGB(screen->format,153,217,234));
        SDL_BlitSurface(menu,NULL,screen,&posMenu);
        SDL_Flip(screen);
        SDL_WaitEvent(&Event);
        switch(Event.type){
            case SDL_QUIT:          //l'utilisateur veut fermer la fen�tre
                InGame = 0;
            break;
            case SDL_KEYUP:   //on choisit KEYDOWN pour la "r�activit�" du programme
                switch(Event.key.keysym.sym){
                    case SDLK_ESCAPE:
                        InGame = 0;     //l'utilisateur a appuy� sur Echap
                    break;
                    case SDLK_KP1:
                        chosenDifficulty=appelDifficulty(screen,posMenu,&InGame,system);
                        if(chosenDifficulty==1){
                            jouer(screen,&InGame,0,Resolution,system);
                        }
                        if(chosenDifficulty==2){
                            jouer(screen,&InGame,200,Resolution,system);
                        }
                    break;
                    case SDLK_KP2:
                        jouer2(screen,&InGame,Resolution,system);
                    break;
                    case SDLK_KP3:
                        appelScores(screen,posMenu,&InGame,Resolution,system);
                    break;
                    case SDLK_KP4:
                        appelButDuJeu(screen,posMenu,&InGame,system);
                    break;
                    case SDLK_KP5:      //Puisqu'on utilise une variable juste pour ce cas, les accolades sont n�cessaires
                        appelParametres(screen,posMenu,&InGame,Resolution,system);
                    break;
                    case SDLK_KP6:
                        appelCredits(screen,posMenu,&InGame,system);
                    break;
                    case SDLK_KP7:  //test des scores
                        InGame = 0;
                    break;
                    case SDLK_p:
                        setMusic(1,1,system);
                    break;
                    default:        //Pour �viter les (nombreux) warnings correspondant aux cas non trait�s (appuis sur touches du clavier, actions sur la souris...)
                        break;
                }
            break;
            case SDL_MOUSEBUTTONDOWN:     //Gestion des clics (gauches) de la souris
                //coordonn�es trouv�es avec le pointeur de Paint
                //cr�dits (il faut prendre en compte le centrage du menu!)
                if (Event.button.button == SDL_BUTTON_LEFT && Event.button.x > (410 + ((Resolution->current_w)/2-(1024/2))) && Event.button.x < (520 + ((Resolution->current_w)/2-(1024/2))) && Event.button.y > (400+((Resolution->current_h)/2-(575/2))) && Event.button.y < (425+((Resolution->current_h)/2-(575/2)))){
                    appelCredits(screen,posMenu,&InGame,system);
                }
                //But du jeu
                if (Event.button.button == SDL_BUTTON_LEFT && Event.button.x > (410 + ((Resolution->current_w)/2-(1024/2))) && Event.button.x < (570 + ((Resolution->current_w)/2-(1024/2))) && Event.button.y > (340+((Resolution->current_h)/2-(575/2))) && Event.button.y < (365+((Resolution->current_h)/2-(575/2)))){
                    appelButDuJeu(screen,posMenu,&InGame,system);
                }
                //Quitter
                if (Event.button.button == SDL_BUTTON_LEFT && Event.button.x > (410 + ((Resolution->current_w)/2-(1024/2))) && Event.button.x < (520 + ((Resolution->current_w)/2-(1024/2))) && Event.button.y > (430+((Resolution->current_h)/2-(575/2))) && Event.button.y < (460+((Resolution->current_h)/2-(575/2)))){
                    InGame = 0;
                }
                //Mode 1 joueur
                if (Event.button.button == SDL_BUTTON_LEFT && Event.button.x > (410 + ((Resolution->current_w)/2-(1024/2))) && Event.button.x < (620 + ((Resolution->current_w)/2-(1024/2))) && Event.button.y > (260+((Resolution->current_h)/2-(575/2))) && Event.button.y < (280+((Resolution->current_h)/2-(575/2)))){
                    chosenDifficulty=appelDifficulty(screen,posMenu,&InGame,system);
                    if(chosenDifficulty==1){
                        jouer(screen,&InGame,0,Resolution,system);
                    }
                    if(chosenDifficulty==2){
                        jouer(screen,&InGame,200,Resolution,system);
                    }
                }
                //Param�tres
                if (Event.button.button == SDL_BUTTON_LEFT && Event.button.x > (410 + ((Resolution->current_w)/2-(1024/2))) && Event.button.x < (570 + ((Resolution->current_w)/2-(1024/2))) && Event.button.y > (370+((Resolution->current_h)/2-(575/2))) && Event.button.y < (400+((Resolution->current_h)/2-(575/2)))){
                    appelParametres(screen,posMenu,&InGame,Resolution,system);
                }
                //Mode 2 joueurs
                if (Event.button.button == SDL_BUTTON_LEFT && Event.button.x > (410 + ((Resolution->current_w)/2-(1024/2))) && Event.button.x < (635 + ((Resolution->current_w)/2-(1024/2))) && Event.button.y > (280+((Resolution->current_h)/2-(575/2))) && Event.button.y < (310+((Resolution->current_h)/2-(575/2)))){
                    jouer2(screen,&InGame,Resolution,system);
                }
                //Highscores
                if (Event.button.button == SDL_BUTTON_LEFT && Event.button.x > (410 + ((Resolution->current_w)/2-(1024/2))) && Event.button.x < (570 + ((Resolution->current_w)/2-(1024/2))) && Event.button.y > (310+((Resolution->current_h)/2-(575/2))) && Event.button.y < (340+((Resolution->current_h)/2-(575/2)))){
                    appelScores(screen,posMenu,&InGame,Resolution,system);
                }
            break;
        }
    }
//    setMusic(0);    //pas vraiment n�cessaire, mais est pr�sent pour fermer "proprement" le jeu
    setMusic(0,0,system);
    SDL_FreeSurface(menu);
    SDL_Quit();
    TTF_Quit();

    return EXIT_SUCCESS;
}
